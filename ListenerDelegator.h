#ifndef LISTENERDELEGATOR_H
#define LISTENERDELEGATOR_H

#include "Messenger/Listener.h"

class CMessage;

template< class DelegateType > 
class TListenerDelegator : public CListener
{
public:
	typedef void ( DelegateType::*FPush )( CMessage& );

public:
					TListenerDelegator( DelegateType& rDelegate, FPush pPushFunction );
	virtual			~TListenerDelegator()	{}
	virtual void	Push( CMessage& rMessage );
	DelegateType&	GetDelegate() const			{ return m_rDelegate; }
	FPush			GetPushFunction() const		{ return m_pPushFunction; }

private:
	TListenerDelegator< DelegateType >&	operator=( 
											const TListenerDelegator< DelegateType >& );

private:
	DelegateType&	m_rDelegate;
	FPush			m_pPushFunction;
};

template< class DelegateType > 
class TListenerDelegator2 : public CListener
{
public:
	typedef void ( DelegateType::*FPush )();

public:
					TListenerDelegator2( DelegateType& rDelegate, FPush pPushFunction );
	virtual			~TListenerDelegator2()	{}
	virtual void	Push( CMessage& );
	DelegateType&	GetDelegate() const			{ return m_rDelegate; }
	FPush			GetPushFunction() const		{ return m_pPushFunction; }

private:
	TListenerDelegator2< DelegateType >&	operator=( 
											const TListenerDelegator2< DelegateType >& );

private:
	DelegateType&	m_rDelegate;
	FPush			m_pPushFunction;
};

template< class DelegateType > 
class TListenerDelegator3 : public CListener
{
public:
	typedef void ( DelegateType::*FPush )( CMessage&, CMessenger& );

public:
					TListenerDelegator3( DelegateType& rDelegate, FPush pPushFunction );
	virtual			~TListenerDelegator3()	{}
	virtual void	Push( CMessage& rMessage, CMessenger& rMessenger );
	DelegateType&	GetDelegate() const			{ return m_rDelegate; }
	FPush			GetPushFunction() const		{ return m_pPushFunction; }

private:
	TListenerDelegator3< DelegateType >&	operator=( 
											const TListenerDelegator3< DelegateType >& );

private:
	DelegateType&	m_rDelegate;
	FPush			m_pPushFunction;
};

template< class DelegateType > 
class TListenerDelegator4 : public CListener
{
public:
	typedef void ( DelegateType::*FPush )() const;
	
public:
	TListenerDelegator4( DelegateType& rDelegate, FPush pPushFunction );
	virtual			~TListenerDelegator4()	{}
	virtual void	Push( CMessage& );
	DelegateType&	GetDelegate() const			{ return m_rDelegate; }
	FPush			GetPushFunction() const		{ return m_pPushFunction; }
	
private:
	TListenerDelegator4< DelegateType >&	operator=( const TListenerDelegator4< DelegateType >& );
	
private:
	DelegateType&	m_rDelegate;
	FPush			m_pPushFunction;
};

#endif // LISTENERDELEGATOR_H

template< class DelegateType > 
TListenerDelegator< DelegateType >::TListenerDelegator( 
	DelegateType& rDelegate, FPush pPushFunction )
	: m_rDelegate( rDelegate )
	, m_pPushFunction( pPushFunction )
{
}

template< class DelegateType > 
void TListenerDelegator< DelegateType >::Push( CMessage& rMessage )	
{ 
	( m_rDelegate.*m_pPushFunction )( rMessage ); 
}

template< class DelegateType > 
TListenerDelegator< DelegateType >&	
	TListenerDelegator< DelegateType >::operator=( 
	const TListenerDelegator< DelegateType >& )
{
	assert( false );
	return *this;
}

template< class DelegateType > 
TListenerDelegator2< DelegateType >::TListenerDelegator2( 
	DelegateType& rDelegate, FPush pPushFunction )
	: m_rDelegate( rDelegate )
	, m_pPushFunction( pPushFunction )
{
}

template< class DelegateType > 
void TListenerDelegator2< DelegateType >::Push( CMessage& )
{ 
	( m_rDelegate.*m_pPushFunction )(); 
}

template< class DelegateType > 
TListenerDelegator2< DelegateType >&	
	TListenerDelegator2< DelegateType >::operator=( 
	const TListenerDelegator2< DelegateType >& )
{
	assert( false );
	return *this;
}

template< class DelegateType > 
TListenerDelegator3< DelegateType >::TListenerDelegator3( 
	DelegateType& rDelegate, FPush pPushFunction )
	: m_rDelegate( rDelegate )
	, m_pPushFunction( pPushFunction )
{
}

template< class DelegateType > 
void TListenerDelegator3< DelegateType >::Push( CMessage& rMessage, 
   CMessenger& rMessenger )
{ 
	( m_rDelegate.*m_pPushFunction )( rMessage, rMessenger ); 
}

template< class DelegateType > 
TListenerDelegator3< DelegateType >&	
	TListenerDelegator3< DelegateType >::operator=( 
	const TListenerDelegator3< DelegateType >& )
{
	assert( false );
	return *this;
}

template< class DelegateType > 
TListenerDelegator4< DelegateType >::TListenerDelegator4( 
	DelegateType& rDelegate, FPush pPushFunction )
	: m_rDelegate( rDelegate )
	, m_pPushFunction( pPushFunction )
{
}

template< class DelegateType > 
void TListenerDelegator4< DelegateType >::Push( CMessage& )
{ 
	( m_rDelegate.*m_pPushFunction )(); 
}

template< class DelegateType > 
TListenerDelegator4< DelegateType >&	
	TListenerDelegator4< DelegateType >::operator=( 
	const TListenerDelegator4< DelegateType >& )
{
	assert( false );
	return *this;
}
